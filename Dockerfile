FROM node:12.9.0-alpine as builder

COPY . .

RUN npm ci --silent

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

RUN npm run build

FROM node:12.9.0-alpine

WORKDIR /usr/src/app

COPY package*.json ./
COPY --from=builder build build

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

RUN npm ci --silent
RUN npm i -g serve

CMD ["serve", "build"]
